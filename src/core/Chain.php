<?php


namespace eqbSaveEvidence\core;
use eqbSaveEvidence\utils\Utils;

class Chain
{


    /**
     * 创建证据链
     * @param $sceneTemplateId
     * @param string $sceneName
     * @param array $linkIds
     * @return array
     */
    public static function creatEviChain($sceneTemplateId,$sceneName='',$linkIds = []){
        //设置构建证据链参数
        $param = [
            "sceneName"=>$sceneName,
            "sceneTemplateId"=>$sceneTemplateId,
            "linkIds"=>$linkIds,
        ];
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return Utils::httpPostData($pa, $signature, PROJECT_ID , VOUCHER_API);

    }

    /**
     * 创建 基础版 存证证据点
     * @param $filePath
     * @param $segmentTempletId
     * @param array $formData
     * @return array
     */
    public static function creatEviSpotBasics($filePath,$segmentTempletId,$formData = []){
        //设置创建证据点参数
        $param = [
            "segmentTempletId" => $segmentTempletId,
            "segmentData"=>json_encode($formData),
            "content"=>[
                "contentDescription"=>basename($filePath),
                "contentLength"=>filesize($filePath),
                "contentBase64Md5"=>Utils::getContentBase64Md5($filePath)
            ]
        ];
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return Utils::httpPostData($pa, $signature, PROJECT_ID , ORIGINAL_STANDARD_API);
    }


    /**
     * 创建 高级 存证证据点
     * @param $filePath
     * @param $segmentTempletId
     * @param array $formData
     * @return array
     */
    public static function creatEviSpotsenior($filePath,$segmentTempletId,$formData = []){
        $param = [
            "segmentTempletId" => $segmentTempletId,
            "segmentData"=>json_encode($formData),
            "content"=>[
                "contentDescription"=>basename($filePath),
                "contentLength"=>filesize($filePath),
                "contentBase64Md5"=>Utils::getContentBase64Md5($filePath)
            ]
        ];
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
       return Utils::httpPostData($pa, $signature, PROJECT_ID , ORIGINAL_ADVANCED_API);
    }



    /**
     * 创建 摘要 存证证据点
     * @param $filePath
     * @param $segmentTempletId
     * @param array $formData
     * @return array
     */
    public static function creatEviSpotAbstract($filePath,$segmentTempletId,$formData = []){
        //设置创建证据点参数
        $param = [
            "segmentTempletId" => $segmentTempletId,
            "segmentData"=>json_encode($formData),
            "content"=>[
                "contentDescription"=>basename($filePath),
                "contentDigest"=>hash_file('sha256',$filePath,false),
            ]
        ];
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return Utils::httpPostData($pa, $signature, PROJECT_ID , ORIGINAL_DIGEST_API);
    }



    /**
     * 根据url 上传文件
     * @param $fileUploadUrl
     * @param $filePath
     * @return array
     */
    public static function uploadFile($fileUploadUrl,$filePath){
        $fileContent = file_get_contents($filePath);
        $contentBase64Md5 = Utils::getContentBase64Md5($filePath);
        $status = utils::sendHttpPUT($fileUploadUrl, $contentBase64Md5, $fileContent);
        if ($status == 200) {
            return ['status' => 'success', 'code' => $status, 'msg' => '待保全文档上传成功'];
        }
        return ['status' => 'error', 'code' => $status, 'msg' => '待保全文档上传失败'];
    }


    /**
     * 将存证证据点追加到证据链中
     * @param $sceneId
     * @param $evid
     * @param array $linkIds
     * @return array
     */
    public static function addEviChain($sceneId,$evid,$linkIds = []){
        //设置行业类型参数
        $param = array(
            "evid"=>$sceneId,
            'linkIds' => $linkIds
        );
        $pa=json_encode($param);
        $signature =  Utils::getSignature($pa,PROJECT_SECRET);
        return  Utils::httpPostData($pa, $signature, PROJECT_ID , VOUCHER_APPEND_API);
    }
}